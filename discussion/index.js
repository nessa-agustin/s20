// console.log('hello world');

/*  WHILE loop
    - takes a single condition. If the condition is true, it will run the code

    Syntax:
        while(condition){
            statement
        }

*/

// let count = 5;

// while (count !== 0) {
//     console.log('While: ' + count );
//     count--;
// }

// let num =0;

// while (num <= 5) {
//     console.log('While: ' + num);
//     num++;
// }



let numA = 0;

// while(numA <= 30){
//     console.log("While: " + numA);
//     numA += 2;

// }


/* 
    Do While
        - works a lot like the while loop but ublike while loop, do-while loops guarantee that the code will be executed at least once.

        Syntax:
            do {
                statement
            }while(condtion)
*/


// let number = Number(prompt("Give me a number?"));

// do {
//     console.log("Do While" + number);
//     number++;
// }while(number < 10)


// let number = Number(prompt("Give me a another number?"));

// do {
//     console.log("Do While" + number);
//     number--;
// }while(number > 10)

/* 
    For loop
        - more flexible than while loop and do-while loop

        -parts:
            -initial value: track progress of loop
            -condition: if true then it will run, if false it will stop the iteration
            -iteration: indicates how to advance the loop, whether to increase or decrease; final expression


        Syntax:
            for(initialValue; condition; iteration){
                statement
            }
*/

// for(let count = 0; count <= 20; count++){
//     console.log("For loop count:" + count);
// }


// let myString = "nessa agustin";
// console.log(myString.length);

// console.log(" ");
// console.log(myString[0]);


// for(let x = 0; x < myString.length; x++){
//     console.log(myString[x]);
// }

let myName = 'JOSEPHINE';

// for(let n =0; n < myName.length; n++){

//     if(
//         myName[n].toLowerCase() == "a" || 
//         myName[n].toLowerCase() == "e" || 
//         myName[n].toLowerCase() == "i" || 
//         myName[n].toLowerCase() == "o" || 
//         myName[n].toLowerCase() == "u"
//         )
//         {
//             console.log("Vowel")
//         }else{
//             console.log(myName[n]);
//         }
  
// }

let str = 'extravagant';
let cons = '';

for(let x = 0; x < str.length ; x++){

    let char = str[x].toLowerCase();
    let isVowel = (char == "a" || char == "e" || char == "i" || char == "o" || char == "u") ? true : false;

    if(!isVowel){

    // }else{
        cons += char;
    }

}

console.log(cons)

/* 
    continue - statement allows the code to go tothe next iteration withour finishing the exexution fo all the statements in the xcode vlock

    break statement on the other hand is keyword that ends the execution of the code or the current loop
*/


let nameA = 'Alexander';

for(let i =0 ; i < nameA.length; i++){
    console.log(nameA[i]);

    if(nameA[i].toLowerCase() === 'a'){
        console.log('Continue to iteration');
        continue;
    }

    if(nameA[i].toLowerCase() === 'd'){
        break;
        console.log('Continue to iteration');
    }
}


